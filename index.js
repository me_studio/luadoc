const chalk = require('chalk');
const escapeStringRegexp = require('escape-string-regexp');
const fs = require('fs-extra');
const path = require('path');

exports.MesLuaDoc = function (luaProjectPath) {
	luaProjectPath = luaProjectPath.trim();
	luaProjectPath = luaProjectPath.replace(/\/$/, "");

	let listFiles = [];
	let docsDirectory = luaProjectPath + '/docs';
	let assetsDirectory = `${docsDirectory}/assets`;
	let docsFilesDirectory = docsDirectory + '/src';
	let mainDocsFile = `${docsDirectory}/index.html`;

	//  Удалить папку с документацией
	if (fs.existsSync(docsDirectory)) {
		deleteFolderRecursive(docsDirectory);
	}

	fs.mkdirSync(docsDirectory);
	fs.mkdirSync(docsFilesDirectory);
	fs.mkdirSync(assetsDirectory);

	let assets = './assets';

	if (fs.existsSync(assets)) {
		fs.copy(assets, assetsDirectory);
	} else {
		fs.copy('./node_modules/luadoc/assets', assetsDirectory);
	}

	let stylesPath = `./styles.css`;
	let styles;

	if (fs.existsSync(stylesPath)) {
		styles = fs.readFileSync(stylesPath, 'UTF-8');
	} else {
		styles = fs.readFileSync(`./node_modules/luadoc/styles.css`, 'UTF-8');
	}

	let htmlTopCode = getHead('Documentation');

	let htmlBottomCode = `
                        </div>
                        <div id="powered-by">
                            <a href="http://metal-evolution.com/" target="_blank">
                                <div class="wrap">
                                    <span>Powered by ©MetalEvolutionStudio</span>
                                    <img src="http://metal-evolution.com/global-assets/images/mes-logo.png" alt="©MetalEvolutionStudio">
                                </div>
                            </a>
                        </div>
                    </body>
                </html>
                `;

	//  Add header part of file
	fs.appendFileSync(mainDocsFile, htmlTopCode);
	startListDocFilesOnMainPage();

	let fileContent = '';
	let fileContentLines = [];
	let currFileI = 0;

	readDirectory(luaProjectPath, writeDoc);
	endListDocFilesOnMainPage();

	//  Add bottom part of file
	fs.appendFileSync(mainDocsFile, htmlBottomCode);

	function readDirectory(directoryPath, handle) {
		let files = fs.readdirSync(directoryPath);

		files.forEach(file => {
			let objPath = directoryPath + '/' + file;

			//  Проверим, это файл или директория
			//
			//  Файл с раширением .lua - добавим в очередь обработки
			let fileStat = fs.lstatSync(objPath);

			if (fileStat.isFile()) {
				if (path.extname(objPath) === '.lua') {
					listFiles.push(objPath);

					handle(objPath);
				}
			}

			//  Если это директория, ищем в ней .lua файлы
			if (fileStat.isDirectory()) {
				readDirectory(objPath, handle);
			}
		});
	}

	function writeDoc(filePath) {
		console.log(chalk.blue(`${filePath}`));

		fileContent = fs.readFileSync(filePath, 'UTF-8');
		let fileComments = fileContent.match(/--\[====\[([\s\S])*?--\]====\]/g);

		//  Пропустить файлы без документации
		if (fileComments === null) {
			console.log(chalk.yellow(`\tNo docs`));
			return;
		}

		let commentHtml = `
                           <div class="back">
                                <a href="../index.html">Back</a>
                           </div>
                           <div class="file">
                                <div class="fileTitle">
                                    <div>${filePath}</div>
                                </div>
                           <div class="contentList">
                            `;

		splitFileContentOnLines();

		currFileI++;
		let currFileName = currFileI + '.html';

		let htmlTopCode = getHead(path.basename(filePath));

		//  Add header part of file
		fs.appendFileSync(`${docsFilesDirectory}/${currFileName}`, htmlTopCode);
		addDocFileOnMainPage(filePath, currFileName);

		let fileHaveErrors = false;

		fileComments.forEach(comment => {
			//  Check if this really doc comment
			let isDocComment = comment.match(/(@type)|(@name)|(@param)|(@brief)/g);

			if (isDocComment === null) {
				return;
			}

			let emptyName = false,
				emptyBrief = false;

			let type = comment.match(/@type (.*)/g);

			//  Имя функции/переменной
			let fullName;
			let name = comment.match(/@name (.*)/g);

			if (name !== null) {
				fullName = name[0];
				name = name[0];
				name = name.replace(/@name/, '');
				name = name.trim();
			} else {
				emptyName = true;
				name = `<span class="empty_name">empty_name</span>`;
			}

			//  What return
			let whatReturn = comment.match(/@return (.*)/g);
			let whatReturnType = '';
			let returnHtml = '';

			if (whatReturn !== null) {
				whatReturn = whatReturn[0];
				whatReturn = whatReturn.replace(/@return/, '');
				whatReturn = whatReturn.trim();
				whatReturnType = whatReturn.match(/\S+(\s+)/g);

				if (whatReturnType !== null) {
					whatReturnType = whatReturnType[0].trim();
				} else {
					whatReturnType = '';
				}

				whatReturn = whatReturn.replace(whatReturnType, '');
				whatReturn = whatReturn.trim();
				returnHtml = `
                               <div class="return">
                                    <div class="return-type">
                                        <span class="return-title">Return</span>
                                        <span class="return-type-title">${whatReturnType}</span>
                                    </div>
                                    <div class="return-desc">
                                        ${whatReturn}    
                                    </div>
                                </div>
                             `;
			} else {
				whatReturn = ''
			}

			//  Краткое описание
			let brief = comment.match(/@brief (.*)/g);

			if (brief !== null) {
				brief = brief[0];
				brief = brief.replace(/@brief/, '');
				brief = brief.trim();
			} else {
				emptyBrief = true
			}

			//  Полное описание
			let fullDescription = comment.match(/@description\s+\[([\n\r](.*))+(?:\s|[\n\r]])/g);

			if (fullDescription !== null) {
				fullDescription = fullDescription[0];
				fullDescription = fullDescription.replace(/@description\s\[/, '');
				fullDescription = fullDescription.replace(/]$/gm, '');
				fullDescription = fullDescription.trim();
			} else {
				fullDescription = '';
			}

			//  Параметры
			let params = comment.match(/@param (.*)/g);
			let paramsHtml = '';

			if (params !== null) {
				params.forEach(param => {
					param = param.replace(/@param/, '');
					param = param.trim();

					let paramName = param.match(/(.*?\s)/i);

					if (paramName !== null) {
						paramName = paramName[0].trim();
					} else {
						paramName = param.match(/(.*)/i)[0];
					}

					paramName = paramName.split(':');
					let paramType = paramName[1];
					paramName = paramName[0];

					paramName = `<span class="param-name">${paramName}</span><span class="param-type">${paramType}</span>`;

					let paramDesc = param.match(/\s(.*)/i);

					if (paramDesc === null) {
						paramDesc = '';
					} else {
						paramDesc = paramDesc[0].trim();
					}

					paramsHtml += `
                            <div class="item">
                                <div class="name">
                                    ${paramName}
                                </div>
                                <div class="description">
                                    ${paramDesc}
                                </div>
                            </div>`;
				});
			}

			commentHtml += `<div class="item">
                    <div class="top">
                        <div class="name">${name}</div>
                    </div>
                    <div class="params">
                        ${paramsHtml}
                    </div>
                    ${returnHtml}
                    <div class="description">
                        <div class="brief">
                            ${brief}
                        </div>
                        <div class="full">
                            ${fullDescription}                        
                        </div>
                    </div>
                </div>`;

			let haveErrors = false;
			let errorMessage = '';

			if (emptyName) {
				haveErrors = true;
				errorMessage += '\t  + Didn\'t find @name or empty\r\n';
			}

			if (emptyBrief) {
				haveErrors = true;
				errorMessage += '\t  + Didn\'t find @brief or empty\r\n'
			}

			if (haveErrors) {
				fileHaveErrors = true;
				let onLine = findLine(comment);
				errorMessage = `\tErrors on line ${onLine}:\r\n` + errorMessage;
				console.log(chalk.red(errorMessage));
			}
		});

		if (!fileHaveErrors) {
			console.log(chalk.green('\tGood'));
		}

		commentHtml += '</div></div>';

		fs.appendFileSync(`${docsFilesDirectory}/${currFileName}`, commentHtml);
		fs.appendFileSync(`${docsFilesDirectory}/${currFileName}`, htmlBottomCode);
	}

	/**
	 * Remove folder recursive
	 * @param path
	 */
	function deleteFolderRecursive(path) {
		if (fs.existsSync(path)) {
			fs.readdirSync(path).forEach(function (file, index) {
				var curPath = path + "/" + file;
				if (fs.lstatSync(curPath).isDirectory()) { // recurse
					deleteFolderRecursive(curPath);
				} else { // delete file
					fs.unlinkSync(curPath);
				}
			});
			fs.rmdirSync(path);
		}
	}

	/**
	 * Разбить новый содержимое фала по строчкам
	 */
	function splitFileContentOnLines() {
		fileContentLines = fileContent.split('\n');
	}

	/**
	 * Найти номер строки в файле.
	 *
	 * @param need Что нужно искать
	 * @returns integer|null
	 */
	function findLine(need) {
		need = escapeStringRegexp(need);
		let needList = need.split('\n');
		let needI = 0;

		for (let key in fileContentLines) {
			let line = fileContentLines[key];
			let currNeed = needList[needI];
			let reg = new RegExp(`(.*)${currNeed}(.*)`);
			let result = reg.exec(line);

			if (result !== null) {
				needI++;

				if (needI === needList.length) {
					return key - needList.length + 2;
				}
			} else {
				needI = 0;
			}
		}

		return null;
	}

	/**
	 * Start list doc files on main page
	 */
	function startListDocFilesOnMainPage() {
		let html = `<div class="list-files">`;

		fs.appendFileSync(mainDocsFile, html);
	}

	/**
	 * End list doc files on main page
	 */
	function endListDocFilesOnMainPage() {
		let html = `
                    </div>
                    `;

		fs.appendFileSync(mainDocsFile, html);
	}

	/**
	 * Add document file link on main page list
	 *
	 * @param filePath File path
	 * @param realName Real file name
	 */
	function addDocFileOnMainPage(filePath, realName) {
		let html = `
                    <div class="file-item">
                        <a href="./src/${realName}">${filePath}</a>
                    </div>
                    `;
		fs.appendFileSync(mainDocsFile, html);
	}

	/**
	 * Get document head with title
	 * @returns {string}
	 */
	function getHead(title) {
		if (title === undefined) {
			title = 'Documentation';
		}

		return `
                <html>
                    <head>
                        <meta charset="utf-8" />
                        <title>${title}</title>
                        <style>
                            ${styles}
                        </style>
                       </head>
                    <body>
                        <div class="documentationWrap">
                `;
	}
};
