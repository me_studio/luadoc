# LuaDoc

This package created for generate documentation for Lua language. This package is meant to be one movement to simplify your life.

Create *js* file with similar code

```javascript
const MesLuaDoc = require('luadoc');

MesLuaDoc.MesLuaDoc('.')
```

And for example we have .lua file with documentation

```javascript
local M = {}

--[====[
	@type func
	@name libUrl.encodeURI(str)
	@param str:string Input string
	@return string Encoded string
	@brief Brie description
	@description [
		Multiline comments!
	]
--]====]
M.encodeURI = function(str)
  str = string.gsub(str, "\r?\n", "\r\n")

  str = string.gsub(str, "([^%w%-%.%_%~ ])",
    function(c) return string.format("%%%02X", string.byte(c)) end)

  --Convert spaces to plus signs
  str = string.gsub(str, " ", "%%20")

  return str
end

return M
```

Attention! You need have multiline comments
`--[====[ Your docs --]====]`

And now run doc generator!

[![lua-doc-cli.png](https://s11.postimg.org/6smb6gd8z/lua-doc-cli.png)](https://postimg.org/image/hs7ii23nz/)

Program create *docs* folder with documentation

[![lua-doc-docs.png](https://s13.postimg.org/cqg4px1kn/lua-doc-docs.png)](https://postimg.org/image/486olkv1v/)
