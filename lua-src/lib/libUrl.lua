local M = {}

--[====[
	@type func
	@name libUrl.encodeURI(str)
	@param str:string Input string
	@return string Encoded string
	@brief Brie description
	@description [
		Multiline comments!
	]
--]====]
M.encodeURI = function(str)
  str = string.gsub(str, "\r?\n", "\r\n")

  str = string.gsub(str, "([^%w%-%.%_%~ ])",
    function(c) return string.format("%%%02X", string.byte(c)) end)

  --Convert spaces to plus signs
  str = string.gsub(str, " ", "%%20")

  return str
end

return M