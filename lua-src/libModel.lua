--[====[
	@type func
	@name libModel.setHost(host)
	@param host:string Host, for example, http://localhost:4000/
	@brief Set host
--]====]

local M = {}

M.setHost = function(host)
	M.host = host
end

return M
